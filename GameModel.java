import java.util.Queue;

public class GameModel {
	
	////     Attributes
	
	private Tile[] tiles;
	
	private Scene[] cards;
	
	private int daysLeft;
	
	private Queue<Player> players;
	
	private Player currentPlayer;
	
	private GetComponent xmlParser;
	
	////     Constructor
	
	public GameModel(int playerCount) {}
	
	////     Methods
	
	public Tile[] getTiles() {return null;}
	
	public Scene[] getCards() {return null;}
	
	public int getDaysLeft() {return -1;}
	
	public Player getCurrentPlayer() {return null;}
	
	public void decreaseDaysLeft() {}
	
	public int getRemainingSets() {return -1;}
}
