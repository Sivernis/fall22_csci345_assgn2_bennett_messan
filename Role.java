class Role
{
    private String name;
    private String quote;
    private int rank;
    private boolean occupied;
    private boolean  complete;

    public Role(String name, String quote, int rank)
    {
        this.name = name;
        this.quote = quote;
        this.rank = rank;
        occupied = false;
        complete = false;
    }

    public int getRank() {return -1;}

    public boolean  isOccupied() {return false;}
    public void setOcupied(boolean  occupied) {}
    
    public boolean  isComplete() {return false;}
    public void complete() {}

    public void reset() {}



}