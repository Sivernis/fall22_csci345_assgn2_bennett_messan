
public class Player {

	////     Attributes
	
	private String name;
	private int rank;
	private int dollars;
	private int credits;
	private int practiceChips;
	private Tile currentTile;
	private Role currentRole;
	
	////     Constructor
	
	public Player(String n, int rank, int dollars, int credits, Tile startingTile) {}
	
	////     Methods
	
	public String getName() {return null;}
	
	public String getCurrentTile() {return null;}
	
	public int getCreditAmount() {return -1;}
	
	public int getDollarAmount() {return -1;}
	
	public int getPracticeChipsAmount() {return -1;}
	
	public int getRank() {return -1;}
	
	public int rollDie() {return -1;}
	
	public void addCredits(int credits) {}
	
	public void addDollars(int dollars) {}
	
	public void moveToTile(Tile destination) {}
	
	public void takeRole(Role role) {}
	
	public void act() {}
	
	public void rehearse() {}
	
	public void increaseRank() {}
}
