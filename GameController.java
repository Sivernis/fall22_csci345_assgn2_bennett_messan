
public class GameController {

    ////    Attributes
	
	private GameView view;
	private GameModel model;
	
	////     Constructor
	
	public GameController(GameModel m, GameView v) {}
	
	////     Methods
	
	public void play() {}
	
	private void upgradePlayerRank() {}
	
	private void wrapScene(Set set) {}
	
	private void endDay() {}
	
	private void calculateScore() {}
	
}
