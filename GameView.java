import java.util.ArrayList;

public class GameView {
	
	public GameView() {}
	
	public void printCurrentPlayerInformation(Player currentPlayer) {}
	
	public void printAllPlayersLocation(ArrayList<Player> players) {}
	
	public void printSourceAndDestination(Tile source, Tile destination) {}
	
	public void printCurrentPlayerWork() {}
	
	public void printUpgradeDetails() {}
	
	public void printRehearseResults() {}
	
	public void printActResults() {}
}
